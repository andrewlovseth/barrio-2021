<section class="hero">
    <div class="content">
        <?php $image = get_field('hero_photo'); if( $image ): ?>
            <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
        <?php endif; ?>
    </div>
</section>