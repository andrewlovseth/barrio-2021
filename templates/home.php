<?php

/*

	Template Name: Home

*/

get_header(); ?>

    <?php get_template_part('templates/home/hero'); ?>

    <?php get_template_part('templates/home/details'); ?>

<?php get_footer(); ?>